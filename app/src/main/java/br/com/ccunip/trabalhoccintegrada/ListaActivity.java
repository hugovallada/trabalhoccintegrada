package br.com.ccunip.trabalhoccintegrada;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

    Button novo;
    ArrayList<Pessoa> pessoas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        final ListView lista = (ListView) findViewById(R.id.listaRegistro);
         pessoas = (ArrayList<Pessoa>) getIntent().getSerializableExtra("Array");
        if(pessoas == null){
            pessoas = new ArrayList<Pessoa>();
        }
        ArrayAdapter<Pessoa> pAdapter = new ArrayAdapter<Pessoa>(this, android.R.layout.simple_list_item_1, pessoas);
        lista.setAdapter(pAdapter);

        novo = (Button) findViewById(R.id.novoRegistro);
        novo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent novo = new Intent(ListaActivity.this,NovoRegistroActivity.class);
                novo.putExtra("Array",pessoas);
                finish();
                startActivity(novo);

            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pessoa p = (Pessoa) parent.getItemAtPosition(position);
                Toast.makeText(ListaActivity.this, p.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
