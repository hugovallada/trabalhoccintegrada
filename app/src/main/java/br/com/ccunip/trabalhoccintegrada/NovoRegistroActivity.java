package br.com.ccunip.trabalhoccintegrada;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class NovoRegistroActivity extends AppCompatActivity {

    EditText nome;
    EditText telefone;
    Button btnCadastra;
    Pessoa pessoa = new Pessoa();
   ArrayList<Pessoa> pessoas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_registro);
            pessoas = (ArrayList<Pessoa>) getIntent().getSerializableExtra("Array");

        if(pessoas == null){
            pessoas = new ArrayList<Pessoa>();


        }

        btnCadastra = (Button) findViewById(R.id.btnCadastra);
        btnCadastra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                    if (pessoas.size() <= 9) {
                        nome = (EditText) findViewById(R.id.edtNome);
                        telefone = (EditText) findViewById(R.id.edtTelefone);

                        if (nome.getText().toString().length() < 4) {
                            Toast.makeText(NovoRegistroActivity.this, "O nome deve ter pelo menos 4 caracteres", Toast.LENGTH_SHORT).show();
                        } else if (telefone.getText().length() < 8) {
                            Toast.makeText(NovoRegistroActivity.this, "O telefone deve ter pelo menos 8 caracteres", Toast.LENGTH_SHORT).show();
                        } else {
                            pessoa.setNome(nome.getText().toString());
                            pessoa.setTelefone(telefone.getText().toString());
                            pessoas.add(pessoa);

                            Intent lista = new Intent(NovoRegistroActivity.this, MainActivity.class);
                            lista.putExtra("Array", pessoas);
                            finish();
                            startActivity(lista);
                        }


                    } else {
                        Toast.makeText(NovoRegistroActivity.this, "Você já atingiu o limite máximo de cadastro", Toast.LENGTH_SHORT).show();
                    }

                }
        });


    }
}
