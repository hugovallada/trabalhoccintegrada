package br.com.ccunip.trabalhoccintegrada;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button btnNovo;
    Button btnLista;
    ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //pessoas = (ArrayList<Pessoa>) getIntent().getSerializableExtra("Array");


        btnNovo = (Button) findViewById(R.id.btnNovoRegistro);
        btnNovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent novo = new Intent(MainActivity.this,NovoRegistroActivity.class);
                novo.putExtra("Array",pessoas);
                startActivity(novo);
            }
        });

        btnLista = (Button) findViewById(R.id.btnListarRegistro);
        btnLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lista = new Intent(MainActivity.this,ListaActivity.class);
                lista.putExtra("Array",pessoas);
                startActivity(lista);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        pessoas = (ArrayList<Pessoa>) getIntent().getSerializableExtra("Array");


        btnLista = (Button) findViewById(R.id.btnListarRegistro);
        btnLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lista = new Intent(MainActivity.this,ListaActivity.class);
                lista.putExtra("Array",pessoas);
                finish();
                startActivity(lista);
            }
        });
    }
}
