package br.com.ccunip.trabalhoccintegrada;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hugo on 09/04/16.
 */
public class Pessoa implements Serializable {
    private String nome;
    private String telefone;

    public Pessoa(String nome, String telefone) {
        this.nome = nome;
        this.telefone = telefone;
    }

    public Pessoa() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return "Nome: " + this.getNome() + "\nTelefone: " + this.getTelefone();
    }
}
